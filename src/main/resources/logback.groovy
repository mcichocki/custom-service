import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender

import static ch.qos.logback.classic.Level.ALL
import static ch.qos.logback.classic.Level.WARN

appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = '%date{ISO8601} %-5level [%thread] - %msg%n'
    }
}

logger('uk.co.cichocki', ALL, ['CONSOLE'], false)

root(WARN, ['CONSOLE'])