package uk.co.cichocki

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.web.SpringBootServletInitializer

/**
 * Translated from Java by user on 25/07/2016.
 */
@SpringBootApplication
class First extends SpringBootServletInitializer {

    private static final Class<First> APPLICATION_CLASS = First

    static void main(String[] args) {
        SpringApplication.run(First, args)
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        application.sources(APPLICATION_CLASS)
    }

}
