package uk.co.cichocki.controller

import com.wordnik.swagger.annotations.ApiOperation
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import uk.co.cichocki.model.GenericResponse

/**
 * Created by user on 12/07/2016.
 */
@RestController
@Slf4j
@PropertySource('classpath:custom-service.properties')
class MainController {

    @Value('${custom-service.main-controller.message.ok}')
    String messageOk

    @RequestMapping(value = '/simple',
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(value = 'simple test')
    @ResponseBody
    GenericResponse simpleTest() {
        GenericResponse response = new GenericResponse(messageOk)
        log.trace "${this.class.simpleName} responded with $response"
        response
    }

}
