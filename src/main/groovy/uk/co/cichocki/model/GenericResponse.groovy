package uk.co.cichocki.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.ToString

/**
 * Created by user on 19/07/2016.
 */
@ToString
class GenericResponse {

    String message
    private final Date date

    GenericResponse() {
    }

    GenericResponse(String message) {
        this.message = message
        this.date = new Date()
    }

    @JsonIgnore
    String getDateFormatted() {
        date.format('ddMMyyyy-hh:mm:ss')
    }
}
