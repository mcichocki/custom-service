import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification
import uk.co.cichocki.First
import uk.co.cichocki.controller.MainController
import uk.co.cichocki.model.GenericResponse

/**
 * Created by user on 20/07/2016.
 */
@WebAppConfiguration
@ContextConfiguration(classes = [First])
class MainControllerSpec extends Specification {

    MockMvc mockMvc

    @Autowired
    WebApplicationContext webApplicationContext

    @Autowired
    MainController mainController

    void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    }

    def simpleTest() {
        given:
        String url = '/simple'

        when:
        String response = mockMvc.perform(
                MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response.contentAsString
        then:
        GenericResponse genericResponse = new ObjectMapper().readValue(response, GenericResponse)
        genericResponse.message == mainController.messageOk
    }
}